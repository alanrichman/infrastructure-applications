# Enable Cloud Resource Manager API
resource "google_project_service" "crm_api" {
  project = var.project_id
  service = "cloudresourcemanager.googleapis.com"
}

# Enable GKE API
resource "google_project_service" "container_api" {
  project = var.project_id
  service = "container.googleapis.com"

  depends_on = [
    google_project_service.crm_api
  ]
}

# Enable Compute Engine API
resource "google_project_service" "compute_api" {
  project = var.project_id
  service = "compute.googleapis.com"

  depends_on = [
    google_project_service.crm_api
  ]
}
