# `infrastructure/create`

Create a VPC, subnet, and cluster for deploying [Podinfo](https://github.com/stefanprodan/podinfo)
