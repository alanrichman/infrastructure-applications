variable "name" {
  type = string
}

variable "project_id" {
  type = string
}

variable "region" {
  type = string
}

variable "gke_num_nodes" {
  type    = number
  default = 1
}
