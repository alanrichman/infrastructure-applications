resource "flux_bootstrap_git" "infrastructure_applications" {
  depends_on = [gitlab_deploy_key.infrastructure_applications]

  path = "applications"
}
