resource "tls_private_key" "flux" {
  algorithm   = "ECDSA"
  ecdsa_curve = "P256"
}

data "gitlab_project" "infrastructure_applications" {
  path_with_namespace = var.gitlab_project_path_with_namespace
}

resource "gitlab_deploy_key" "infrastructure_applications" {
  title    = "${var.name}-deploy-key"
  project  = data.gitlab_project.infrastructure_applications.id
  key      = tls_private_key.flux.public_key_openssh
  can_push = true
}
