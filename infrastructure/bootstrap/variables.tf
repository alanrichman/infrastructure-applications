variable "name" {
  type = string
}

variable "gitlab_token" {
  sensitive = true
  type      = string
}

variable "gitlab_project_path_with_namespace" {
  type = string
}
