# `infrastructure/bootstrap`

Bootstrap the cluster created in `infrastructure/create` for use with [Flux CD](https://fluxcd.io/)
