terraform {
  required_version = ">=1.1.5"

  required_providers {
    flux = {
      source  = "fluxcd/flux"
      version = ">= 1.2.3"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">=16.10.0"
    }
  }
}

provider "gitlab" {
  token = var.gitlab_token
}

provider "flux" {
  kubernetes = {
    config_path = "~/.kube/config"
  }
  git = {
    url = "ssh://git@gitlab.com/${data.gitlab_project.infrastructure_applications.path_with_namespace}.git"
    ssh = {
      username    = "git"
      private_key = tls_private_key.flux.private_key_pem
    }
  }
}
