Based on https://cert-manager.io/docs/tutorials/getting-started-with-cert-manager-on-google-kubernetes-engine-using-lets-encrypt-for-ingress-ssl/

Per the instructions, this secret needs to be manually applied:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: web-tls
  namespace: podinfo
type: kubernetes.io/tls
stringData:
  tls.key: ""
  tls.crt: ""
```

Unlike the instructions we are using GitOps, so we can apply this whenever in the process and Flux will reconcile it. 
